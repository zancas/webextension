ZingO-Labs is an ideologically motivated collective.  Our creations are our best efforts to make
more just and open systems.   As such we develop with core values in mind:

(0)  Knowledge is not exclusive, our systems are open source.
