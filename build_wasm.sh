#! /usr/bin/env bash

set -ex

TARGET=target/wasm32-unknown-unknown/debug
cargo build --target wasm32-unknown-unknown
rm -rf wasms
wasm-bindgen --out-name bindgens.wasm --out-dir ./wasms --target no-modules ${TARGET}/zingo_webextension.wasm
rm -f src/bindgens.js
mv wasms/bindgens.js src/
