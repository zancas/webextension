mod background;
mod content;

#[cfg(test)]
pub mod tests {
    use super::*;

    #[test]
    fn checkdb() {
        let db = curationdb::load_curation_db();
        let expected_value: u64 = 1;
        let observed_value = db.get("https://www.youtube.com/user/foofighters");
        assert_eq!(Some(&expected_value), observed_value);
    }
}
