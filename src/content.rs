use wasm_bindgen::closure::Closure;
use wasm_bindgen::{JsCast, JsValue};
use wasm_bindgen_macro::wasm_bindgen;
use web_extension;
use web_sys;
mod curationdb;

#[wasm_bindgen]
pub fn content() {
    console_error_panic_hook::set_once();
    get_zaddr();
    let local_db = curationdb::load_curation_db();
    let window = web_sys::window().expect("no global window object");
    let document = window.document().expect("window has no document");
    let results = document
        .get_elements_by_class_name("results")
        .item(0)
        .expect("results element does not exist");
    let dom_manip = move || {
        let length = results.children().length();
        let mut result_vec = Vec::new();
        for i in 0..length {
            if results
                .children()
                .item(i)
                .expect("ddg result removed during race")
                .id()
                .starts_with("r1-")
            {
                result_vec.push((results.children().item(i).unwrap(), 0));
            }
        }
        let mut reordered_results = Vec::new();
        result_vec.iter().for_each(|(element, _zero)| {
            let weight = local_db.get(&find_url(element));
            if weight != None {
                reordered_results.push((element, *weight.unwrap() as u64));
            } else {
                reordered_results.push((element, 0));
            }
        });
        reordered_results.sort_by(|(_e1, weight1), (_e2, weight2)| weight2.cmp(weight1));
        reordered_results.iter().for_each(|x| {
            results.append_child(&x.0).unwrap();
        });
        results
            .append_child(&results.children().named_item("rld-1").unwrap())
            .expect("failed to append child");
    };
    if document.ready_state() == "complete" {
        dom_manip();
    } else {
        window.set_onload(Some(
            Closure::once_into_js(dom_manip).as_ref().unchecked_ref(),
        ));
    }
    ()
}

fn find_url(element: &web_sys::Element) -> String {
    element
        .first_child_of_class("result__body links_main links_deep")
        .first_child_of_class("result__extras js-result-extras")
        .first_child_of_class("result__extras__url")
        .text_content()
        .unwrap()
}
trait ChildrenByClass {
    fn first_child_of_class(&self, class: &str) -> web_sys::Element;
}

impl ChildrenByClass for web_sys::Element {
    fn first_child_of_class(&self, class: &str) -> web_sys::Element {
        for child in 0..self.children().length() {
            if self.children().item(child).unwrap().class_name() == class {
                return self.children().item(child).unwrap();
            }
        }
        panic!("No child of class {}", class);
    }
}
fn get_zaddr() {
    let ear = Closure::wrap(Box::new(move |message: &JsValue| {
        web_sys::console::log_1(message);
    }) as Box<dyn Fn(&JsValue)>);
    let runtime = &(*web_extension::browser).runtime();
    runtime
        .on_message()
        .add_listener(ear.as_ref().unchecked_ref());
    ear.forget();
    runtime.send_message(None, &JsValue::from_str("active"), None);
}
