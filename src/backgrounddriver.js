const drive_background_script = async () => {
    await wasm_bindgen(browser.runtime.getURL("./wasms/bindgens.wasm"));
    wasm_bindgen.background()
}
drive_background_script();
