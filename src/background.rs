use wasm_bindgen::closure::Closure;
use wasm_bindgen::{JsCast, JsValue};
use wasm_bindgen_macro::wasm_bindgen;
use web_extension;
use web_sys;

#[wasm_bindgen]
pub fn background() {
    web_sys::console::log_1(&"background is in".into());
    let test_client = zcashclient::Client::new();
    let runtime = web_extension::Browser::runtime(&*web_extension::browser);
    let reciever = Closure::wrap(Box::new(move |message: JsValue, sender: JsValue| {
        web_sys::console::log_1(&message);
        let address = &test_client.address();
        let m: String = message.as_string().unwrap_or("invalid".to_string());
        let answer = match m.as_str() {
            "active" => JsValue::from(address),
            _ => JsValue::from("You failed"),
        };
        let tabs = web_extension::Browser::tabs(&*web_extension::browser);
        tabs.send_message(
            web_extension::MessageSender::from(sender)
                .tab()
                .unwrap()
                .id()
                .unwrap(),
            &(answer),
            None,
        );
    }) as Box<dyn Fn(JsValue, JsValue)>);
    runtime
        .on_message()
        .add_listener(reciever.as_ref().unchecked_ref());
    reciever.forget();
}
