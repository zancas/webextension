import * as fs from 'fs';
import { JSDOM } from "jsdom";
import test from "ava";
//import { _get_raw_DDG_search_results,
//         _extract_DDG_URLS,
//         ingest_DDG_search_URLS,
//         request_endorsement_dictionary,
//         generate_unranked_matches_array,
//         process_search_URLS,
//         executeOnload } from '../src/ddg_search_results';

test.beforeEach('create a document mock', t => {
    const aslk_data =
        fs.readFileSync('./test/data/aslk_at_DuckDuckGo.html', 'utf8');
    const mockdom = new JSDOM(aslk_data);
    //const mockdom = {"getElementById": t => {console.log(t);}};
    t.context.mockdom = mockdom;
})
test('executeOnload self test', t => {
    t.is(executeOnload, executeOnload);
});

test.only('_get_raw_DDG_search_results', t => {
    // This function is a thin wrapper around a call to document... so
    // this test will serve as a POC for mocking document.
    const mockdom = t.context.mockdom;
    const results = mockdom.window.document.getElementsByClassName("results js-results");
    console.log(results[0].textContent);
    console.log(results.length);
    console.log(results);
    t.true(true);
});
